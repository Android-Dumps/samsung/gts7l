## gts7lxx-user 13 TP1A.220624.014 T875XXU2DWB2 release-keys
- Manufacturer: samsung
- Platform: kona
- Codename: gts7l
- Brand: samsung
- Flavor: gts7lxx-user
- Release Version: 13
- Kernel Version: 4.19.113
- Id: TP1A.220624.014
- Incremental: T875XXU2DWB2
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: undefined
- Fingerprint: samsung/gts7lxx/gts7l:11/RP1A.200720.012/T875XXU2DWB2:user/release-keys
- OTA version: 
- Branch: gts7lxx-user-13-TP1A.220624.014-T875XXU2DWB2-release-keys
- Repo: samsung/gts7l
