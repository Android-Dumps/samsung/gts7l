#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_gts7l.mk

COMMON_LUNCH_CHOICES := \
    lineage_gts7l-user \
    lineage_gts7l-userdebug \
    lineage_gts7l-eng
