#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_gts7l.mk

COMMON_LUNCH_CHOICES := \
    omni_gts7l-user \
    omni_gts7l-userdebug \
    omni_gts7l-eng
